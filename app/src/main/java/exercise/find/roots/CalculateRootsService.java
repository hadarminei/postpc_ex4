package exercise.find.roots;

import android.app.IntentService;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.util.Log;

public class CalculateRootsService extends IntentService {

  int MAX_TIME = 20;

  public CalculateRootsService() {
    super("CalculateRootsService");
  }

  /**
   * Function creates intent in case the calculation was aborted
   * @param numberToCalculateRootsFor the number we tried to calculate roots for
   * @param timeStartMs - time the calculation took
   */
  public void ranOutOfTime(long numberToCalculateRootsFor, long timeStartMs) {
    Log.e("ranout", "ran out of time");
    Intent calculate_intent = new Intent("stopped_calculations");
    calculate_intent.putExtra("time_until_give_up_seconds", (System.currentTimeMillis() - timeStartMs)/1000);
    sendBroadcast(calculate_intent);
  }

  /**
   * Function creates intent in case the calculation was successful
   * @param root1 - the first root of the number
   * @param root2 - - the second root of the number
   * @param numberToCalculateRootsFor - the number we tried to calculate roots for
   * @param timeStartMs - time the calculation took
   */
  public void foundRoots(long root1, long root2, long numberToCalculateRootsFor, long timeStartMs) {
    Intent calculate_intent = new Intent("found_roots");
    calculate_intent.putExtra("root1", root1);
    calculate_intent.putExtra("root2", root2);
    calculate_intent.putExtra("time", (System.currentTimeMillis()-timeStartMs)/1000);
    calculate_intent.putExtra("original_number", numberToCalculateRootsFor);
    sendBroadcast(calculate_intent);
  }

  @Override
  protected void onHandleIntent(Intent intent) {
    if (intent == null) return;
    long timeStartMs = System.currentTimeMillis();
    long numberToCalculateRootsFor = intent.getLongExtra("number_for_service", 0);
    if (numberToCalculateRootsFor <= 0) {
      Log.e("CalculateRootsService", "can't calculate roots for non-positive input" + numberToCalculateRootsFor);
      return;
    }
    long root1 = 1;
    long root2 = numberToCalculateRootsFor;
    for(int i = 2; i < numberToCalculateRootsFor; i++) {
      if((System.currentTimeMillis() - timeStartMs)/1000 == MAX_TIME) {
        ranOutOfTime(numberToCalculateRootsFor, timeStartMs);
      }
      if(numberToCalculateRootsFor % i == 0) {
        root1 = i;
        root2 = numberToCalculateRootsFor/i;
        break;
      }
    }
    foundRoots(root1, root2, numberToCalculateRootsFor, timeStartMs);
  }

}