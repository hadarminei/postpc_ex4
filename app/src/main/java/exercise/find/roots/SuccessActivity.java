package exercise.find.roots;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import org.w3c.dom.Text;

public class SuccessActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_success);
        TextView resultOutput = findViewById(R.id.resultOutput);
        TextView timeOutput = findViewById(R.id.timeOutput);

        Intent intentSuccess = getIntent();
        if(intentSuccess.hasExtra("root1") && intentSuccess.hasExtra("root2") && intentSuccess.hasExtra("original_number")) {
            long original_number  = intentSuccess.getLongExtra("original_number", -1);
            long root1  = intentSuccess.getLongExtra("root1", -1);
            long root2  = intentSuccess.getLongExtra("root2", -1);
            String result = original_number + "=" + root1 + "*" + root2;
            resultOutput.setText(result);
        }

        if(intentSuccess.hasExtra("time")) {
            long time = intentSuccess.getLongExtra("time", -1);
            String time_took = time + " seconds";
            timeOutput.setText(time_took);
        }
    }
}
