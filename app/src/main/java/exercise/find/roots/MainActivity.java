package exercise.find.roots;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity {

  private BroadcastReceiver broadcastReceiverForSuccess = null;
  private BroadcastReceiver broadcastReceiverForFailure = null;
  boolean negative = false;
  boolean isInProgress = false;
  int counter = 0;

  /**
   * Function checks the validity of the text
   * @param text - the text written
   * @return true if the text is valid and false otherwise
   */
  boolean checkValidity(String text) {
    if(text.isEmpty()) {
      negative = false;
    }
    else if(text.contains("-")) {
        negative = true;
      }
    try {
      long number = Long.parseLong(text);
    }
    catch (NumberFormatException e) {
      return false;
    }
    return true;
    };

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);

    ProgressBar progressBar = findViewById(R.id.progressBar);
    EditText editTextUserInput = findViewById(R.id.editTextInputNumber);
    Button buttonCalculateRoots = findViewById(R.id.buttonCalculateRoots);

    // set initial UI:
    progressBar.setVisibility(View.GONE); // hide progress
    editTextUserInput.setText(""); // cleanup text in edit-text
    editTextUserInput.setEnabled(true); // set edit-text as enabled (user can input text)
    buttonCalculateRoots.setEnabled(false); // set button as disabled (user can't click)

    // set listener on the input written by the keyboard to the edit-text
    editTextUserInput.addTextChangedListener(new TextWatcher() {
      public void beforeTextChanged(CharSequence s, int start, int count, int after) { }
      public void onTextChanged(CharSequence s, int start, int before, int count) { }
      public void afterTextChanged(Editable s) {
        // text did change
        String newText = editTextUserInput.getText().toString();

        //if the input number was a negative number
        buttonCalculateRoots.setEnabled(checkValidity(newText) && !negative);
      }
    });

    // set click-listener to the button
    buttonCalculateRoots.setOnClickListener(v -> {
      isInProgress = true;
      Intent intentToOpenService = new Intent(MainActivity.this, CalculateRootsService.class);
      String userInputString = editTextUserInput.getText().toString();

      // We checked that the number is valid before
      long userInputLong = Long.parseLong(userInputString);
      intentToOpenService.putExtra("number_for_service", userInputLong);
      startService(intentToOpenService);

      // Enable button and text window, show the progress bar
      buttonCalculateRoots.setEnabled(false);
      editTextUserInput.setEnabled(false);
      progressBar.setVisibility(View.VISIBLE);

    });

    // Register a broadcast-receiver to handle action "found_roots"
    broadcastReceiverForSuccess = new BroadcastReceiver() {
      @Override
      public void onReceive(Context context, Intent incomingIntentSuccess) {
        if (incomingIntentSuccess == null || !incomingIntentSuccess.getAction().equals("found_roots")) {
          return;
        }
        // set the screen
        isInProgress = false;
        buttonCalculateRoots.setEnabled(true);
        editTextUserInput.setEnabled(true);
        progressBar.setVisibility(View.GONE);

        // success finding roots!
        onReceiveSuccess(incomingIntentSuccess);
        }
    };
    registerReceiver(broadcastReceiverForSuccess, new IntentFilter("found_roots"));

    // Register a broadcast-receiver to handle action "stopped_calculation"
    broadcastReceiverForFailure = new BroadcastReceiver() {
      @Override
      public void onReceive(Context context, Intent incomingIntentFail) {
        if (incomingIntentFail == null || !incomingIntentFail.getAction().equals("stopped_calculations")) {
          return;
        }
        // set the screen
        Log.e("fail", "failed");
        isInProgress = false;
        buttonCalculateRoots.setEnabled(true);
        editTextUserInput.setEnabled(true);
        progressBar.setVisibility(View.GONE);
        onReceiveFailure(incomingIntentFail);

      }
    };
    registerReceiver(broadcastReceiverForFailure, new IntentFilter("stopped_calculations"));
  }

  /**
   * Function handles the event of a successful calculation
   * @param incomingIntentSuccess - The intent received from the service
   */
  public void onReceiveSuccess(Intent incomingIntentSuccess) {
    // Get all extras from intent
    if (incomingIntentSuccess.hasExtra("root1") && incomingIntentSuccess.hasExtra("root2")
            && incomingIntentSuccess.hasExtra("original_number") && incomingIntentSuccess.hasExtra("time")) {
      long root1 = incomingIntentSuccess.getLongExtra("root1", -1);
      long root2 = incomingIntentSuccess.getLongExtra("root2", -1);
      long original_number = incomingIntentSuccess.getLongExtra("original_number", -1);
      long time = incomingIntentSuccess.getLongExtra("time", -1);

      // Send extras to new intent
      Intent intentToOpenSuccessScreen = new Intent(MainActivity.this, SuccessActivity.class);
      intentToOpenSuccessScreen.putExtra("root1", root1);
      intentToOpenSuccessScreen.putExtra("root2", root2);
      intentToOpenSuccessScreen.putExtra("original_number", original_number);
      intentToOpenSuccessScreen.putExtra("time", time);

      // start the success activity
      startActivity(intentToOpenSuccessScreen);
    }
  }

  /**
   * Function handles the event of a failed calculation
   * @param incomingIntentFail - The intent received from the service
   */
  public void onReceiveFailure(Intent incomingIntentFail) {
    // Get all extras from intent
    if(incomingIntentFail.hasExtra("time_until_give_up_seconds")) {
      long time = incomingIntentFail.getLongExtra("time_until_give_up_seconds", 0);
      Toast.makeText(MainActivity.this, "calculation aborted after "
              + time + " seconds", Toast.LENGTH_SHORT).show();
    }
  }

  @Override
  protected void onDestroy() {
    super.onDestroy();
    this.unregisterReceiver(broadcastReceiverForSuccess);
    this.unregisterReceiver(broadcastReceiverForFailure);
  }

  @Override
  protected void onSaveInstanceState(@NonNull Bundle outState) {
    super.onSaveInstanceState(outState);
    outState.putBoolean("negative", negative);
    EditText editTextUserInput = findViewById(R.id.editTextInputNumber);
    String text = editTextUserInput.getText().toString();
    outState.putString("text", text);
    outState.putBoolean("progress", isInProgress);
  }

  /**
   * Function handles changing the screen's widget when the progress is running
   */
  private void isInProgress() {
    Button buttonCalculatorRoots = findViewById(R.id.buttonCalculateRoots);
    ProgressBar progressBar = findViewById(R.id.progressBar);
    EditText editTextUserInput = findViewById(R.id.editTextInputNumber);

    buttonCalculatorRoots.setEnabled(false);
    editTextUserInput.setEnabled(false);
    progressBar.setVisibility(View.VISIBLE);
  }

  @Override
  protected void onRestoreInstanceState(@NonNull Bundle savedInstanceState) {
    super.onRestoreInstanceState(savedInstanceState);
    String text = savedInstanceState.getString("text");
    negative = savedInstanceState.getBoolean("negative");
    EditText editTextUserInput = findViewById(R.id.editTextInputNumber);
    editTextUserInput.setText(savedInstanceState.getString("text"));
    isInProgress = savedInstanceState.getBoolean("progress");

    if(isInProgress) {
      isInProgress();
    }
  }

}
