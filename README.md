I pledge the highest level of ethical principles in support of academic excellence.
I ensure that all of my work reflects my own abilities and not those of someone else.
Hadar Minei

What would you change in the code in order to let the service run for maximum 200ms in tests environments,
but continue to run for 20sec max in the real app (production environment)?

For changing the running time only for test enviroment we can add to the test a variable to specify
the running time. This is what it would look like: @Test(timeout=200).
The app will stay the same way that we implemented it.
